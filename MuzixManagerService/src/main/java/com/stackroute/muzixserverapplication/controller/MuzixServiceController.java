package com.stackroute.muzixserverapplication.controller;

import java.util.List;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.stackroute.muzixserverapplication.domain.Muzix;
import com.stackroute.muzixserverapplication.exception.MuzixAlreadyExistsException;
import com.stackroute.muzixserverapplication.exception.MuzixNotFoundException;
import com.stackroute.muzixserverapplication.service.MuzixService;

import io.jsonwebtoken.Jwts;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping(path = "/api/v1/muzixService")
public class MuzixServiceController {

	private MuzixService muzixService;
	
	String userId="Tamoghna";

	@Autowired
	public MuzixServiceController(final MuzixService muzixService) {
		this.muzixService = muzixService;
	}

	@PostMapping("/muzix")
	public ResponseEntity<?> saveNewMovie(@RequestBody final Muzix muzix, HttpServletRequest request,
			HttpServletResponse response) {
		ResponseEntity<?> responseEntity;
		/*String authHeader = request.getHeader("authorization");
		String token = authHeader.substring(7);
		String userId = Jwts.parser().setSigningKey("secretkey").parseClaimsJws(token).getBody().getSubject();*/
		try {
			/*muzix.setId(muzix.getId());
			muzix.setUserId(userId);*/
			muzixService.saveMuzix(muzix);
			responseEntity = new ResponseEntity<Muzix>(muzix, HttpStatus.CREATED);
		} catch (MuzixAlreadyExistsException e) {
			responseEntity = new ResponseEntity<String>("{ \"message\":\"" + e.getMessage() + "\"}",
					HttpStatus.CONFLICT);
		}
		return responseEntity;
	}

	/*@PutMapping(path = "/muzix/{id}")
	public ResponseEntity<?> updateMovie(@PathVariable("id") final Integer id, @RequestBody Muzix muzix) {
		ResponseEntity<?> responseEntity;
		try {
			final Muzix fetchedMovie = muzixService.updateMuzix(muzix);
			responseEntity = new ResponseEntity<Muzix>(fetchedMovie, HttpStatus.OK);
		} catch (MuzixNotFoundException e) {
			responseEntity = new ResponseEntity<String>("{ \"message\":\"" + e.getMessage() + "\"}",
					HttpStatus.CONFLICT);
		}
		return responseEntity;
	}*/

	@DeleteMapping(value = "/muzix/{id}/{userId}")
	public ResponseEntity<?> deleteMovieById(@PathVariable("id") String id,@PathVariable("userId") String userId) {
		ResponseEntity<?> responseEntity;
		try {
			muzixService.deleteMuzixById(id,userId);
		} catch (MuzixNotFoundException e) {
			responseEntity = new ResponseEntity<String>("{ \"message\":\"" + e.getMessage() + "\"}",
					HttpStatus.NOT_FOUND);
		}
		responseEntity = new ResponseEntity<String>("", HttpStatus.OK);
		return responseEntity;
	}

	@GetMapping(path = "/muzix/{id}")
	public ResponseEntity<?> fetchMovieById(@PathVariable("id") String id) {
		ResponseEntity<?> responseEntity;
		Muzix thisMovie = null;
		try {
			thisMovie = muzixService.getMuzixById(id);
		} catch (MuzixNotFoundException e) {
			responseEntity = new ResponseEntity<String>(e.getMessage(), HttpStatus.NOT_FOUND);
		}
		responseEntity = new ResponseEntity<Muzix>(thisMovie, HttpStatus.OK);
		return responseEntity;
	}

	@GetMapping("/muzixs/{userId}")
	public ResponseEntity<List<Muzix>> fetchMyMuzixs(@PathVariable("userId") String userId) {
		/*HttpServletRequest request = (HttpServletRequest)req;
		String authHeader = request.getHeader("authorization");
		String token = authHeader.substring(7);
		String userId = Jwts.parser().setSigningKey("secretkey").parseClaimsJws(token).getBody().getSubject();
*/
		return new ResponseEntity<List<Muzix>>(muzixService.getMyMuzixs(userId), HttpStatus.OK);
	}

}
