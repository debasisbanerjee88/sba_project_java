package com.stackroute.muzixserverapplication.domain;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "muzixs")
public class Muzix {

	
	
	
	@Id
	@Column(name = "id")
	//@GeneratedValue(strategy = GenerationType.IDENTITY)
	private String id;
	
	@Column(name = "userId")
	private String userId="Tamoghna";
	
	@Column(name = "muzix_id")
	private String muzixId;
	@Column(name = "name")
	private String name;
	@Column(name = "type")
	private String type;
	@Column(name = "artistName")
	private String artistName;
	@Column(name = "description")
	private String description;
	@Column(name = "previewUrl")
	private String previewURL;
	@Column(name = "imageUrl")
	private String imageUrl;


	
	
	

	public Muzix() {
	}

	public Muzix(String id,String userId,String name, String type, String artistName, String description,
			String previewURL, String imageUrl) {
		
		this.id = id;
		this.userId = userId;
		this.name = name;
		this.type= type;
		this.artistName = artistName;
		this.description = description;
		this.previewURL = previewURL;
		this.imageUrl = imageUrl;
		
	}

	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	@JsonProperty("userId")
	public String getUserId() {
		return userId;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	@JsonProperty("type")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@JsonProperty("artistName")
	public String getArtistName() {
		return artistName;
	}

	public void setArtistName(String artistName) {
		this.artistName = artistName;
	}

	@JsonProperty("description")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@JsonProperty("previewURL")
	public String getPreviewUrl() {
		return previewURL;
	}

	public void setPreviewUrl(String previewURL) {
		this.previewURL = previewURL;
	}

	@JsonProperty("imageUrl")
	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	/*public void setMovieId(String movieId) {
		this.muzixId = movieId;
	}

	
	public String getMovieId() {
		return muzixId;
	}*/

	
	@Override
	public String toString() {
		return "Muzix [id=" + id + ",userId=" + userId + ", name=" + name + ", type=" + type + ", artistName=" + artistName
				+ ", description=" + description + ", previewURL=" + previewURL + ", imageUrl=" + imageUrl + "]";
	}

}
