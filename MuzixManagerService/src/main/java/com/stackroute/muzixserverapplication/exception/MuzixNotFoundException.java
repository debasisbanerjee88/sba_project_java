package com.stackroute.muzixserverapplication.exception;

public class MuzixNotFoundException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public MuzixNotFoundException(String message) {
		super();
		this.message = message;
	}

	@Override
	public String toString() {
		return "MuzixNotFoundException [message=" + message + "]";
	}
}
