package com.stackroute.muzixserverapplication.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.stackroute.muzixserverapplication.domain.Muzix;

public interface MuzixRepository extends JpaRepository<Muzix, String> {

	List<Muzix> findByUserId(String userId);
	
	List<Muzix> findByIdAndUserId(String id,String userId);
}
