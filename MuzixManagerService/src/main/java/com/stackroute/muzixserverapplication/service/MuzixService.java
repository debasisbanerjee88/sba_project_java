package com.stackroute.muzixserverapplication.service;

import java.util.List;

import com.stackroute.muzixserverapplication.domain.Muzix;
import com.stackroute.muzixserverapplication.exception.MuzixAlreadyExistsException;
import com.stackroute.muzixserverapplication.exception.MuzixNotFoundException;

public interface MuzixService {
	boolean saveMuzix(Muzix muzix) throws MuzixAlreadyExistsException;

	//Muzix updateMuzix(Muzix muzix) throws MuzixNotFoundException;

	boolean deleteMuzixById(String id, String userId) throws MuzixNotFoundException;

	Muzix getMuzixById(String id) throws MuzixNotFoundException;

	List<Muzix> getMyMuzixs(String userId);
}
