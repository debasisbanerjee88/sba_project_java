package com.stackroute.muzixserverapplication.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stackroute.muzixserverapplication.domain.Muzix;
import com.stackroute.muzixserverapplication.exception.MuzixAlreadyExistsException;
import com.stackroute.muzixserverapplication.exception.MuzixNotFoundException;
import com.stackroute.muzixserverapplication.repository.MuzixRepository;

@Service
public class MuzixServiceImpl implements MuzixService {

	private final transient MuzixRepository movieRepo;

	@Autowired
	public MuzixServiceImpl(MuzixRepository movieRepo) {
		super();
		this.movieRepo = movieRepo;
	}

	@Override
	public boolean saveMuzix(final Muzix muzix) throws MuzixAlreadyExistsException {
		final Optional<Muzix> object = movieRepo.findById(muzix.getId());
		if (object.isPresent()) {
			throw new MuzixAlreadyExistsException("Could not save Muzix,Muzix already exists!");
		}
		movieRepo.save(muzix);
		return true;
	}

	/*@Override
	public Muzix updateMuzix(final Muzix updateMovie) throws MuzixNotFoundException {
		final Muzix muzix = movieRepo.findById(updateMovie.getId()).orElse(null);
		if (muzix == null) {
			throw new MuzixNotFoundException("Could not update Muzix,Muzix not found!");
		}
		muzix.setComments(updateMovie.getComments());
		movieRepo.save(muzix);
		return muzix;
	}*/

	@Override
	public boolean deleteMuzixById(String id,String userId) throws MuzixNotFoundException {
		List<Muzix> muzix = movieRepo.findByIdAndUserId(id,userId);
		if (muzix == null) {
			throw new MuzixNotFoundException("Could not delete Muzix,Muzix not found!");
		}
		movieRepo.deleteAll(muzix);
		return true;
	}

	@Override
	public Muzix getMuzixById(String id) throws MuzixNotFoundException {
		final Muzix muzix = movieRepo.findById(id).get();
		if (muzix == null) {
			throw new MuzixNotFoundException("Muzix not found!");
		}
		return muzix;
	}

	@Override
	public List<Muzix> getMyMuzixs(String userId) {
		return movieRepo.findByUserId(userId);
	}
}
