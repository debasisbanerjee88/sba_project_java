package com.stackroute.muzixserverapplication.controller;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.stackroute.muzixserverapplication.controller.MuzixServiceController;
import com.stackroute.muzixserverapplication.domain.Muzix;
import com.stackroute.muzixserverapplication.service.MuzixService;

@RunWith(SpringRunner.class)
@WebMvcTest(MuzixServiceController.class)

public class MuzixControllerTest {

	private MockMvc mvc;
	
	@MockBean
	private transient MuzixService service;
	@InjectMocks
	private MuzixServiceController controller;
	
	private transient Muzix muzix;
	static List<Muzix> muzixs;

	@Before
	public void SetUp() {
		MockitoAnnotations.initMocks(this);
		muzixs = new ArrayList<>();
		muzix = new Muzix("1234","Ayan123","1", "POC", "goodMuzix", "ww.abc.com", "2015/03/23", "45.5");
		muzixs.add(muzix);
		muzix = new Muzix("2345","Ayan123","2", "POC1", "goodMuzix1", "ww.efg.com", "2018/03/23", "55.5");
		muzixs.add(muzix);
		
		mvc = MockMvcBuilders.standaloneSetup(controller).build();
	}

	@Test
	public void testSaveNewMuzixSucess() throws Exception {
		String token = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJBeWFuMTIzIiwiaWF0IjoxNTM1MDIzNTQzfQ.T4cIFDAPnS0NGU6RWBlzlzLKDwovNc1kMkeUO7rK4JY";
		//muzix.setComments("Not that much Good");
		when(service.saveMuzix(muzix)).thenReturn(true);
		mvc.perform(post("/api/v1/muzixService/muzix")./*header("authorization", "Bearer "+token).*/contentType(MediaType.APPLICATION_JSON).content(jsonToString(muzix)))
				.andExpect(status().isCreated());
		verify(service, times(1)).saveMuzix(Mockito.any(Muzix.class));
		verifyNoMoreInteractions(service);
	}

	/*@Test
	public void testUpdateMuzixSucess() throws Exception {
		//muzix.setComments("Not so Good");
		when(service.updateMuzix(muzix)).thenReturn(muzixs.get(0));
		mvc.perform(put("/api/v1/muzixService/muzix/{id}", 1).contentType(MediaType.APPLICATION_JSON).content(jsonToString(muzix)))
				.andExpect(status().isOk());
		verify(service, times(1)).updateMuzix(Mockito.any(Muzix.class));
		verifyNoMoreInteractions(service);
	}*/

	private static String jsonToString(final Object obj) throws JsonProcessingException {
		// TODO Auto-generated method stub
		String result;
		try {
			final ObjectMapper mapper = new ObjectMapper();
			final String jsonContent = mapper.writeValueAsString(obj);
			result = jsonContent;
		} catch (JsonProcessingException e) {
			// TODO: handle exception
			result = "Json Processing error";
		}
		return result;
	}

	@Test
	public void testDeleteMuzixById() throws Exception {
		when(service.deleteMuzixById("1234","Ayan123")).thenReturn(true);
		mvc.perform(delete("/api/v1/muzixService/muzix/{id}/{userId}", "1234","Ayan123")).andExpect(status().isOk());
		verify(service, times(1)).deleteMuzixById("1234","Ayan123");
		verifyNoMoreInteractions(service);
	}

	@Test
	public void testFetchMuzixById() throws Exception {
		when(service.getMuzixById("1")).thenReturn(muzixs.get(0));
		mvc.perform(get("/api/v1/muzixService/muzix/{id}", 1)).andExpect(status().isOk());
		verify(service, times(1)).getMuzixById("1");
		verifyNoMoreInteractions(service);
	}

	/*@Test
	public void testGetMyMuzixs() throws Exception {
		String token = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJBeWFuMTIzIiwiaWF0IjoxNTM1MDIzNTQzfQ.T4cIFDAPnS0NGU6RWBlzlzLKDwovNc1kMkeUO7rK4JY";
		when(service.getMyMuzixs("Ayan123")).thenReturn(null);
		mvc.perform(get("/api/v1/muzixService/muzixs").header("authorization", "Bearer "+token).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
		verify(service, times(1)).getMyMuzixs("Ayan123");
		verifyNoMoreInteractions(service);
	}*/

}
