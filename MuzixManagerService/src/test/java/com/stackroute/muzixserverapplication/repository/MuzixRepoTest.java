package com.stackroute.muzixserverapplication.repository;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.stackroute.muzixserverapplication.domain.Muzix;
import com.stackroute.muzixserverapplication.repository.MuzixRepository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

//@RunWith(SpringRunner.class)
//@DataJpaTest
//@AutoConfigureTestDatabase(replace = Replace.NONE)
//@Transactional
public class MuzixRepoTest {
	@Autowired
	private transient MuzixRepository repo;

	public void setRepo(final MuzixRepository repo) {
		this.repo = repo;
	}

	//@Test
	public void testSaveMuzix() throws Exception {
		repo.save(new Muzix("1234","Ayan123","1", "POC", "goodMuzix", "ww.abc.com", "2015/03/23", "45.5"));
		final Muzix muzix = repo.getOne("1");
		assertThat(muzix.getId()).isEqualTo(1);
	}

	/*//@Test
	public void testUpdateMuzix() throws Exception {
		repo.save(new Muzix(3456,"Ayan123","1", "POC", "goodMuzix", "ww.abc.com", "2015/03/23", "45.5"));
		final Muzix muzix = repo.getOne(1);
		assertEquals(muzix.getName(), "superman");
		//muzix.setComments("hi");
		repo.save(muzix);
		final Muzix tempMovie = repo.getOne(1);
		assertEquals("hi", tempMovie.getComments());
	}*/

	//@Test
	public void testDeleteMuzix() throws Exception {
		repo.save(new Muzix("1234","Ayan123","1", "POC", "goodMuzix", "ww.abc.com", "2015/03/23", "45.5"));
		final Muzix muzix = repo.getOne("1");
		assertEquals(muzix.getName(), "superman");
		repo.delete(muzix);
		assertEquals(Optional.empty(), repo.findById("1"));
	}

	//@Test
	public void testGetMovie() throws Exception {
		repo.save(new Muzix("1234","Ayan123","1", "POC", "goodMuzix", "ww.abc.com", "2015/03/23", "45.5"));
		final Muzix muzix = repo.getOne("1");
		assertEquals(muzix.getName(), "superman");
	}

	//@Test
	public void testGetMyMuzixs() throws Exception {
		repo.save(new Muzix("1234","Ayan123","1", "POC", "goodMuzix", "ww.abc.com", "2015/03/23", "45.5"));
		repo.save(new Muzix("1234","Ayan123","1", "POC", "goodMuzix", "ww.abc.com", "2015/03/23", "45.5"));
		final List<Muzix> muzixs = repo.findAll();
		assertEquals(muzixs.get(0).getName(), "superman");
	}
}
