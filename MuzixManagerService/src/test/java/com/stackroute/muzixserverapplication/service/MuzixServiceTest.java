package com.stackroute.muzixserverapplication.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.stackroute.muzixserverapplication.domain.Muzix;
import com.stackroute.muzixserverapplication.exception.MuzixAlreadyExistsException;
import com.stackroute.muzixserverapplication.exception.MuzixNotFoundException;
import com.stackroute.muzixserverapplication.repository.MuzixRepository;
import com.stackroute.muzixserverapplication.service.MuzixServiceImpl;

import static org.junit.Assert.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


public class MuzixServiceTest {

	@Mock
	private transient MuzixRepository muzixRepo;
	private transient Muzix muzix;
	@InjectMocks
	private transient MuzixServiceImpl muzixServiceImpl;
	transient Optional<Muzix> options;

	@Before
	public void setupMock() {
		MockitoAnnotations.initMocks(this);
		muzix = new Muzix("1234","Ayan123","1", "POC", "goodMuzix", "ww.abc.com", "2015/03/23", "45.5");
		options = Optional.of(muzix);
	}

	@Test
	public void testMockCreation() {
		assertNotNull("jpaRepository creation fails:use @injectMocks on muzixServiceImpl", muzix);
	}

	@Test
	public void testSavemuzixSuccess() throws MuzixAlreadyExistsException {
		when(muzixRepo.save(muzix)).thenReturn(muzix);
		final boolean flag = muzixServiceImpl.saveMuzix(muzix);
		assertTrue("saving muzix failed,the call to muzixDAOImpl is returning false,check this", flag);
		verify(muzixRepo, times(1)).save(muzix);
	}

	@Test(expected = MuzixAlreadyExistsException.class)
	public void testSavemuzixFailure() throws MuzixAlreadyExistsException {
		when(muzixRepo.findById("1234")).thenReturn(options);
		when(muzixRepo.save(muzix)).thenReturn(muzix);
		final boolean flag = muzixServiceImpl.saveMuzix(muzix);
		assertFalse("saving muzix failed", flag);
		verify(muzixRepo, times(1)).findById(muzix.getId());
	}

	/*@Test
	public void testUpdatemuzix() throws MuzixNotFoundException {
		when(muzixRepo.findById(1)).thenReturn(options);
		when(muzixRepo.save(muzix)).thenReturn(muzix);
		muzix.setComments("not so good muzix");
		final Muzix muzix1 = muzixServiceImpl.updateMuzix(muzix);
		assertEquals("updating muzix failed", "not so good muzix", muzix1.getComments());
		verify(muzixRepo, times(1)).save(muzix);
		verify(muzixRepo, times(1)).findById(muzix.getId());
	}*/

	@Test
	public void testDeletemuzixById() throws MuzixNotFoundException {
		final List<Muzix> muzixList = new ArrayList<>(1);
		when(muzixRepo.findByIdAndUserId("1234","Ayan123")).thenReturn(muzixList);
		doNothing().when(muzixRepo).deleteAll(muzixList);
		final boolean flag = muzixServiceImpl.deleteMuzixById("1234","Ayan123");
		assertTrue("deleting muzix failed", flag);
		verify(muzixRepo, times(1)).deleteAll(muzixList);
		verify(muzixRepo, times(1)).findByIdAndUserId(muzix.getId(),muzix.getUserId());
	}

	@Test
	public void testGetmuzixById() throws MuzixNotFoundException {
		when(muzixRepo.findById("1234")).thenReturn(options);
		final Muzix muzix1 = muzixServiceImpl.getMuzixById("1234");
		assertEquals("fetching muzix by id failed", muzix1, muzix);
		verify(muzixRepo, times(1)).findById(muzix.getId());
	}

	@Test
	public void testGetAllmuzixs() {
		final List<Muzix> muzixList = new ArrayList<>(1);
		when(muzixRepo.findByUserId("Ayan123")).thenReturn(muzixList);
		final List<Muzix> muzixs1 = muzixServiceImpl.getMyMuzixs("Ayan123");
		assertEquals(muzixList, muzixs1);
		verify(muzixRepo, times(1)).findByUserId("Ayan123");
	}

}
