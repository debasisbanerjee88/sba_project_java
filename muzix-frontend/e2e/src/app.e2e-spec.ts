import { AppPage } from './app.po';
import { browser, logging } from 'protractor';
describe('workspace-project App', () => {
  let page: AppPage;
  beforeAll(function(){
    page = new AppPage();
    browser.driver.manage().window().maximize();
    page.navigateTo();
  })
  //start: Register module testing e2e
  it('should click on Register Link', () => {
     
    page.getRegisterLinkElement().click();
    browser.sleep(3000);
    
  });
  it('should write blank name in user name box', () => {
    page.getUserNameTextbox().sendKeys('');
    browser.sleep(5000);
  });
  
  
  it('should write blank name in first name box', () => {
    page.getFirstNameTextbox().sendKeys('');
    browser.sleep(5000);
  });
  
  it('should write blank in last name box', () => {
    page.getLastNameTextbox().sendKeys('');
    browser.sleep(5000);
  });
  
  it('should write blank in password box', () => {
    page.getPasswordTextbox().sendKeys('');
    browser.sleep(5000);
  });
  //fill with value in register page
  it('should write name in user name box', () => {
    page.getUserNameTextbox().sendKeys('Tamoghna');
    browser.sleep(3000);
  });
  
  
  it('should write name in first name box', () => {
    page.getFirstNameTextbox().sendKeys('Tamoghna');
    browser.sleep(3000);
  });
  
  it('should write in last name box', () => {
    page.getLastNameTextbox().sendKeys('Das');
    browser.sleep(3000);
  });
  
  it('should write in password box', () => {
    page.getPasswordTextbox().sendKeys('test');
    browser.sleep(3000);
  });
  it('should click on Register button', () => {
    page.getRegisterButtonElement().click();
    browser.sleep(3000);
  });
  //End: Register module testing e2e
  //start: Login module testing e2e
  it('should write blank name in user name box', () => {
    page.getUserNameTextbox().sendKeys('');
    browser.sleep(3000);
  });
  
  it('should write blank in password box', () => {
    page.getPasswordTextbox().sendKeys('');
    browser.sleep(3000);
  });
  
    it('should write user name in user name box', () => {
      page.getUserNameTextbox().sendKeys('Tamoghna');
      browser.sleep(3000);
    });
  
    it('should write password in password box', () => {
      page.getPasswordTextbox().sendKeys('test');
      browser.sleep(3000);
    });
  
    it('should click on Login button', () => {
      page.getLoginButtonElement().click();
      browser.sleep(3000);
    });
  //End: Login module testing e2e
  //Start: Dashboard  module testing e2e
  it('should click on Home Link', () => {
      page.getHomeLinkElement().click();
      browser.sleep(5000);
  });
  
    it('should Click on Add to fevourite button from Track Section', () => {
      
      page.getTrackAddButtonElement().click();
      browser.sleep(5000);
      
    });
    it('should Click on Remove from Fevourite button from Track section in Home page', () => {
     
      page.getTrackRemoveButtonElement().click();
      browser.sleep(5000);
      
    });
    it('should Click on Add to fevourite button from Track Section', () => {
      
      page.getTrackAddButtonElement().click();
      browser.sleep(5000);
      
    });

    it('should click on Favourite Link', () => {
     
      page.getFavouriteLinkElement().click();
      browser.sleep(5000);
      
    });

    it('should Click on remove from Favourite page', () => {
     
      page.getFavouriteTrackRemoveButtonElement().click();
      browser.sleep(5000);
    });
  
    it('should click on Home Link', () => {
     
      page.getHomeLinkElement().click();
      browser.sleep(5000);
      
    });
    it('should click on Track accordian tab', () => {
     
      page.getTrackAccordianElement().click();
      browser.sleep(5000);
      
    });
    it('should click on Play List accordian tab', () => {
     
      page.getPlayListAccordianElement().click();
      browser.sleep(5000);
      
    });
  
    it('should Click on Add to fevourite button from Play List Section', () => {
     
      page.getPlayListAddButtonElement().click();
      browser.sleep(5000);

    });
    it('should Click on Remove from Fevourite button from Play List in Home page', () => {
     
      page.getPlayListRemoveButtonElement().click();
      browser.sleep(5000);
   
    });

    it('should Click on Add to fevourite button from Play List', () => {
     
      page.getPlayListAddButtonElement().click();
      browser.sleep(5000);

    });
  
    it('should click on Favourite Link', () => {
     
      page.getFavouriteLinkElement().click();
      browser.sleep(5000);
      
    });

    it('should Click on Remove from Fevourite button from Favourite page', () => {
     
      page.getFavouritePlayListRemoveButtonElement().click();
      browser.sleep(5000);
    });
  
    it('should click on Home Link', () => {
     
      page.getHomeLinkElement().click();
      browser.sleep(5000);
      
    });


    it('should submit Search form', () => {
     
      page.getSearchFormTextboxEmement().sendKeys('A R Rehman');
      browser.sleep(5000);
      page.getSeachButtonElement().click();
      browser.sleep(5000);
    });

    it('should Click on Add to fevourite button from Search page', () => {
     
      page.getSearchTrackAddButtonElement().click();
      browser.sleep(5000);
     
    });
    it('should Click on Remove from Fevourite button from Search page', () => {
     
      page.getSearchTrackRemoveButtonElement().click();
      browser.sleep(5000);
     
    });
    

    it('should Click on Log Out button from header', () => {
     
      page.getLogoutButtonElement().click();
      browser.sleep(5000);
    });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
