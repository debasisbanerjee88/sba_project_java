import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get(browser.baseUrl) as Promise<any>;
  }

 //register
 getRegisterLinkElement() {
    
  return element(by.id('registerLink'));
}
getFirstNameTextbox()
{
  return  element(by.name('firstName'));
}
getLastNameTextbox()
{
  return  element(by.name('lastName'));
}
getRegisterButtonElement()
{
  return element(by.buttonText('Register'));
}
//login page
getLoginLinkElement() {
  
  return element(by.id('loginLink'));
}
getUserNameTextbox()
{
  return  element(by.name('userId'));
}
getPasswordTextbox()
{
  return  element(by.name('password'));
}
getLoginButtonElement()
{
  return element(by.buttonText('Login'));
}
//end of Login screen testing 
//start Dashboard
getHomeLinkElement() {
  
  return element(by.id('homeLink'));
}

//Get Top news section button elements
getTrackAddButtonElement()
{
  return element(by.id('TrackBtn_0'));
}
getTrackRemoveButtonElement()
{
  return element(by.id('TrackBtn_0'));
}

//Category News section 
getTrackAccordianElement()
{
  return element(by.id('tracks'));
}
getPlayListAccordianElement()
{
  return element(by.id('playList'));
}

getPlayListAddButtonElement()
{
  return element(by.id('PlayListBtn_0'));
}
getPlayListRemoveButtonElement()
{
  return element(by.id('PlayListBtn_0'));
}
//Favourite section elements 
getFavouriteLinkElement() {
  return element(by.id('favouriteLink'));
}

getFavouriteTrackRemoveButtonElement()
{
  return element(by.id('removeFavBtn_0'));
}
getFavouritePlayListRemoveButtonElement()
{
  return element(by.id('removePlayList_0'));
}
//Favourite section elements 
getSearchFormTextboxEmement()
{
    return  element(by.name('searchText'));
}

getSeachButtonElement()
{
  return  element(by.css('button[type=submit]'));
}

getSearchTrackAddButtonElement()
{
  return element(by.id('SearchBtn_0'));
}
getSearchTrackRemoveButtonElement()
{
  return element(by.id('SearchBtn_0'));
}
//logout button element 
getLogoutButtonElement()
{
  return element(by.id('logout'));
}
}
