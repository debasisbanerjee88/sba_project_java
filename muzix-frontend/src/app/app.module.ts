import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './components/app.component';
import { routingComponents, AppRoutingModule } from './route/app-routing.module';

import { AuthGuard } from './authguard/auth.guard';
import { TokenInterceptorService } from './interceptor/token-interceptor.service';
import { HeaderComponent } from './components/header/header.component';
import { FavouriteService } from './services/favourite.service';
import { MusicsService } from './services/musics.service';
import { PlaylistService } from './services/playlist.service';
import { DashboardComponent } from './components/dashboard/dashboard.component';


@NgModule({
  declarations: [
    AppComponent,
    routingComponents,
    HeaderComponent,
    DashboardComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [ 
    FavouriteService,
    MusicsService,
    PlaylistService,
    AuthGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi:true
    }
],
  bootstrap: [AppComponent]
})

export class AppModule { } 
