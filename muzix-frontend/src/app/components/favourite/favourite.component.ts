import { Component, OnInit } from '@angular/core';
import { FavouriteService } from 'src/app/services/favourite.service';
import { Tracks } from 'src/app/model/tracks';

@Component({
  selector: 'app-favourite',
  templateUrl: './favourite.component.html',
  styleUrls: ['./favourite.component.css']
})
export class FavouriteComponent implements OnInit {
  favourites:Tracks[];
  tracks:Tracks[]=[];
  playlists:Tracks[]=[];
  constructor(private _favouriteService:FavouriteService) { }

  ngOnInit() {
    this.getFavourite();
  }

  public getFavourite()
  {
    this._favouriteService.getFavourite().subscribe(response=>{
      this.favourites=response;
    
      this.tracks=this.favourites.filter(f=>f.type=='track')
      this.playlists=this.favourites.filter(f=>f.type=='playlist')
    },
    err=>{
      alert(err.error);
      }); 
  }
  removeFavourite(track)
  {
    this._favouriteService.removeFavourite(track.id).subscribe(response=>{
      this.getFavourite();
     },
     err=>{
       alert(err.error);
       }); 
  }
 
}
