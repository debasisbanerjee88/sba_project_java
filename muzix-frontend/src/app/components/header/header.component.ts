import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
 isLoggedIn: Boolean;
  isLoggedIn$: Observable<boolean>;

  constructor(private authService: AuthService, private _router: Router) { }

  ngOnInit() {
    this.isLoggedIn$ = this.authService.isLoggedIn;
    this.isLoggedIn$.subscribe(valreturn => this.isLoggedIn = valreturn);
  }

  onLogout() {
    this.authService.logout();
  }

  getSearchItems(searchText)
  {
    if(!!searchText)
  this._router.navigate(['/search',{q:searchText}]);
  } 

}
