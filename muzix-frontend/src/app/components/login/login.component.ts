import { Component, OnInit } from '@angular/core';
import { User } from '../../model/user';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user:User;
tokenStr:string;
  constructor(private _auth:AuthService,private _router:Router) {
    this.user=new User();
   }

  ngOnInit() {
  }

//user login
Login(){
  this._auth.login(this.user);
} 

}
