import { Component, OnInit } from '@angular/core';
import { MusicsService } from 'src/app/services/musics.service';
import { Tracks } from 'src/app/model/tracks';

@Component({
  selector: 'app-musics',
  templateUrl: './musics.component.html',
  styleUrls: ['./musics.component.css']
})
export class MusicsComponent implements OnInit {
  tracks:Tracks[];
  favouriteTracks:Tracks[];
  constructor(private _musicService:MusicsService) { }

  ngOnInit() {
    this.getMusics();
  }


  public getMusics()
  {
    
    this._musicService.getMusics().subscribe(response=>{
     
      this.tracks=response.tracks;
      for(let track of this.tracks)
      track.imageUrl=`http://direct.rhapsody.com/imageserver/v2/albums/${track.albumId}/images/300x300.jpg`
      
      this.getFavourite();
   
    },
    err=>{
      alert(err.error);
      });
  }

  public toggleFavourite(track:Tracks)
  {
    if(!track.isFavourite)
    {
      this._musicService.makeFavourite(track).subscribe(response=>{
       track.isFavourite=true;
      },
      err=>{
        alert(err.error);
        }); 
    }
    else{
      this._musicService.removeFavourite(track.id).subscribe(response=>{
        track.isFavourite=false;
       },
       err=>{
         alert(err.error);
         }); 
    }
  
  }

  public getFavourite()
  {
    this._musicService.getFavourite().subscribe(response=>{
      this.favouriteTracks=response;
     
    
       for(let track of this.tracks)
       {
        for(let ftrack of this.favouriteTracks)
        {
          if(track.id==ftrack.id)
          {
            track.isFavourite=true;
          }
        }
       }
    
    },
    err=>{
      alert(err.error);
      }); 
  }

}
