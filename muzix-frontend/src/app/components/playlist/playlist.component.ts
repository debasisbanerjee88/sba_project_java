import { Component, OnInit } from '@angular/core';
import { PlaylistService } from 'src/app/services/playlist.service';
import { Tracks } from 'src/app/model/tracks';


@Component({
  selector: 'app-playlist',
  templateUrl: './playlist.component.html',
  styleUrls: ['./playlist.component.css']
})
export class PlaylistComponent implements OnInit {
  playlists:any;
  favouriteTracks:Tracks[];
  constructor(private _playListService:PlaylistService) { }

  ngOnInit() {
    this.getPlayList();
  }


  public getPlayList()
  {
    this._playListService.getPlayList().subscribe(response=>{
     
      this.playlists=response.playlists;
      this.getFavourite();
    },
    err=>{
      alert(err.error);
      });
  }

  public getFavourite()
  {
    this._playListService.getFavourite().subscribe(response=>{
      this.favouriteTracks=response;
    
     
       for(let playlist of this.playlists)
       {
        for(let ftrack of this.favouriteTracks)
        {
          if(playlist.id==ftrack.id)
          {
            playlist.isFavourite=true;
          }
        }
       }
    
    },
    err=>{
      alert(err.error);
      }); 
  }

  public toggleFavourite(item:any)
  {
    item.imageUrl=item.images[0].url;
    if(!item.isFavourite)
    {
      this._playListService.makeFavourite(item).subscribe(response=>{
       item.isFavourite=true;
      },
      err=>{
        alert(err.error);
        }); 
    }
    else{
      this._playListService.removeFavourite(item.id).subscribe(response=>{
        item.isFavourite=false;
       },
       err=>{
         alert(err.error);
         }); 
    }
  
  }
}
