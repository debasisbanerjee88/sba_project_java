import { Component, OnInit } from '@angular/core';
import { User } from '../../model/user';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
 user:User;

 constructor(private _auth:AuthService,private _router:Router) {
   this.user=new User();
  }

  ngOnInit() {
  }
  
  //user registration
  RegisterUser(){
    this._auth.register(this.user).subscribe(
      data=>{
        this._router.navigate(['login'])
      },
    err=>{
      alert(err.error);
    });
  }
}
