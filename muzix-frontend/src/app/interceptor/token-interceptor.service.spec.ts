import { TestBed, inject } from '@angular/core/testing';

import { TokenInterceptorService } from './token-interceptor.service';
import { RouterTestingModule } from '../../../node_modules/@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';

describe('TokenInterceptorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TokenInterceptorService],
      imports:[RouterTestingModule,HttpClientModule]
    });
  });

  it('should be created', inject([TokenInterceptorService], (service: TokenInterceptorService) => {
    expect(service).toBeTruthy();
  }));
});
