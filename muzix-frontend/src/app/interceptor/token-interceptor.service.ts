import { Injectable, Injector } from '@angular/core';
import {HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse} from '@angular/common/http'
import { AuthService } from '../services/auth.service';
import { Observable, of } from '../../../node_modules/rxjs';
import { Router } from '@angular/router';
import { catchError } from 'rxjs/operators'; 

@Injectable({
  providedIn: 'root'
})
export class TokenInterceptorService implements HttpInterceptor{

  constructor(private _injector: Injector,private router:Router,private authService:AuthService) { }
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let authservice=this._injector.get(AuthService);
    let header=req.headers.set('Content-Type','application/json');
    
    if(req.url.toLowerCase().indexOf('api/v1/muzixservice')>0)
    {
      header=req.headers.set('Authorization',`Bearer ${authservice.getToken()}`);
    }
    let tokenizeRequest=req.clone(
      {
        headers:header
      });
      
    return next.handle(tokenizeRequest).pipe(
      catchError(
        (err) => {
          if (err.status === 401){
            this.handleAuthError();
            return of(err);
          }
          throw err;
        }
      )
    );
  }
  private handleAuthError() {
     alert('Your session is expired. Please login to continue!')
     this.authService.logout();
  }






}
