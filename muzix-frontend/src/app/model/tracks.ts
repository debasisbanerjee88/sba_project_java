export class Tracks{
    id:string;
    userId:string;
    name:string;
    type:string;
    artistName:string;
    description:string;
    previewURL:string;
    imageUrl:string;
    albumId:string;
    isFavourite:boolean;
   
}