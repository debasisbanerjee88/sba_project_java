import { Injectable } from '@angular/core';
import { User } from '../model/user';
import { environment } from '../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private loggedIn: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor(private _router: Router,private _http: HttpClient) { }
 
  register(user:User)
  {
    let body=JSON.stringify(user);
    
    let registerPostURL=environment._AUTH_BASE+environment._REGISTER;
    return this._http.post<any>(registerPostURL,body,httpOptions);
  }

  login(user:User)
  {
    let body=JSON.stringify(user);
    let loginPostURL=environment._AUTH_BASE+environment._LOGIN;
    return this._http.post<any>(loginPostURL,body,httpOptions).subscribe(
      res=>{
        localStorage.setItem('user', body);
        localStorage.setItem('token', res.token);
        this.loggedIn.next(true);
        this._router.navigate(['/home']);
      },
    err=>{
      alert(err.error);
    });
  }

  logout() {
    localStorage.clear();
    this.loggedIn.next(false);
    this._router.navigate(['/login']);
  }
  
  getToken():string
  {
    return localStorage.getItem('token');
  }

  getLoggedInUser():User
  {
     return JSON.parse(localStorage.getItem('user'));
  }
  
  get isLoggedIn() {
    if(!!this.getToken())
    {
      this.loggedIn.next(true);
    }
    else{
      this.loggedIn.next(false);
    }
    return this.loggedIn.asObservable();
  }

}
