import { TestBed } from '@angular/core/testing';

import { FavouriteService } from './favourite.service';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';

describe('FavouriteService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports:[HttpClientModule,RouterTestingModule]
  }));

  it('should be created', () => {
    const service: FavouriteService = TestBed.get(FavouriteService);
    expect(service).toBeTruthy();
  });
});
