import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Tracks } from '../model/tracks';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { AuthService } from './auth.service';


@Injectable({
  providedIn: 'root'
})
export class FavouriteService {

  constructor(private _auth: AuthService, private _http:HttpClient) { }

  getFavourite():Observable<Tracks[]>
  {
    let currentUser=this._auth.getLoggedInUser();
    let userId=(!!currentUser)?currentUser.userId: null;

    let getFavouriteURL=`${environment.BASE_URL}${environment._GET_FAVOURITE_MUZIX}/${userId}`;
    return this._http.get<Tracks[]>(getFavouriteURL);
  }

  removeFavourite(trackId)
  {
    let currentUser=this._auth.getLoggedInUser();
    let userId=(!!currentUser)?currentUser.userId: null;

    let removeFavURL=`${environment.BASE_URL}${environment._DELETE_FAV_MUZIX}${trackId}/${userId}`;
    
    return this._http.delete(removeFavURL)
  }
}
