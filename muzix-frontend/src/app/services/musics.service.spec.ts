import { TestBed } from '@angular/core/testing';

import { MusicsService } from './musics.service';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';

describe('MusicsService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports:[HttpClientModule,RouterTestingModule]
  }));

  it('should be created', () => {
    const service: MusicsService = TestBed.get(MusicsService);
    expect(service).toBeTruthy();
  });
});
