import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Music } from '../model/music';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Tracks } from '../model/tracks';
import { AuthService } from './auth.service';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class MusicsService {

  constructor(private _http:HttpClient, private _auth: AuthService) { }
  
  getMusics():Observable<Music>
  {
    return this._http.get<Music>('http://api.napster.com/v2.2/tracks/top?apikey=YTkxZTRhNzAtODdlNy00ZjMzLTg0MWItOTc0NmZmNjU4Yzk4')
  }

  makeFavourite(track:Tracks)
  {
    let currentUser=this._auth.getLoggedInUser();
    let userId=(!!currentUser)?currentUser.userId: null;
    track.userId=userId;
    let body=JSON.stringify(track);
    
    let makeFavouriteURL=`${environment.BASE_URL}${environment._ADD_TO_FAVOURITE}`;
    return this._http.post(makeFavouriteURL,body,httpOptions);
  }

  removeFavourite(trackId)
  {
    let currentUser=this._auth.getLoggedInUser();
    let userId=(!!currentUser)?currentUser.userId: null;
     
    let removeFavURL=`${environment.BASE_URL}${environment._DELETE_FAV_MUZIX}${trackId}/${userId}`;
    
    return this._http.delete(removeFavURL);
  }
 
  getFavourite():Observable<Tracks[]>
  {
    let currentUser=this._auth.getLoggedInUser();
    let userId=(!!currentUser)?currentUser.userId: null;

    let getFavouriteURL=`${environment.BASE_URL}${environment._GET_FAVOURITE_MUZIX}/${userId}`;
    
    return this._http.get<Tracks[]>(getFavouriteURL);
  }
}
