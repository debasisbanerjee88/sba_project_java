import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { Tracks } from '../model/tracks';
import { AuthService } from './auth.service';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
@Injectable({
  providedIn: 'root'
})
export class SearchService {

  constructor(private _http:HttpClient, private _auth: AuthService) { }
  
  getSearchItems(searchText)
  {
    let _searchURL=`http://api.napster.com/v2.2/search?apikey=YTkxZTRhNzAtODdlNy00ZjMzLTg0MWItOTc0NmZmNjU4Yzk4&query=${searchText}&type=playlist,track`;
    return this._http.get<any>(_searchURL);
  }

  makeFavourite(item:any)
  {
    let currentUser=this._auth.getLoggedInUser();
    let userId=(!!currentUser)?currentUser.userId: null;
    item.userId=userId;
    let body=JSON.stringify(item);
    let makeFavouriteURL=`${environment.BASE_URL}${environment._ADD_TO_FAVOURITE}`;
    return this._http.post(makeFavouriteURL,body,httpOptions);
  }

  removeFavourite(trackId)
  {
    let currentUser=this._auth.getLoggedInUser();
    let userId=(!!currentUser)?currentUser.userId: null;
   
    let removeFavURL=`${environment.BASE_URL}${environment._DELETE_FAV_MUZIX}${trackId}/${userId}`;
    
    return this._http.delete(removeFavURL)
  }

  getFavourite():Observable<Tracks[]>
  {
    let currentUser=this._auth.getLoggedInUser();
    let userId=(!!currentUser)?currentUser.userId: null;

    let getFavouriteURL=`${environment.BASE_URL}${environment._GET_FAVOURITE_MUZIX}/${userId}`;
    
    return this._http.get<Tracks[]>(getFavouriteURL);
  }
}
